<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
	<link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
	<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
    </head>
    
    <body>
	<h1>EWB USA Rutgers Texting</h1>
	<h2>Make your Selection</h2>
	<form action="index.php" method="post">
		View All Messages 					<input type="radio" name="all" value="messages" /><br />
		View All Mailing Lists 				<input type="radio" name="all" value="mailing"  /><br />
		View All Events 					<input type="radio" name="all" value="events"  /><br />
		View All Members 					<input type="radio" name="all" value="members"  /><br />
		<input type="submit"" />
	</form>
	<form name="input" action="index.php" method="post">
		View Members At a Meeting or on a Mailing List <input type="text" name="members_meetings"/>
		<input type="submit" />
	</form>
	<form name="input" action="index.php" method="post">
		View An Individual Person<input type="text" name="person"/>
		<input type="submit" />
	</form>
	
	
	<?php 
	session_start();
	
	//connect to database and update
	$db_server = '88.198.43.24';
	$db_user = 'ewbrutgers';
	$db_password = '11ruEWB12';
	$db_dbname = 'ewbtexts';
	$db = mysql_pconnect($db_server, $db_user, $db_password);
	mysql_select_db($db_dbname);	
	
	//deciding based off of the input
	//radio buttons
	if ($_POST["all"]!=""){
		switch($_POST["all"]){//which radio button
			case "messages":
				//display messages
				echo "<h2>Messages</h2>";
				echo "<table border='1'>";
				$result = mysql_query("SELECT * FROM emails");
				while($row = mysql_fetch_array($result)){
					echo "<tr><td>";
					echo $row['Phone'] . "<td>" . $row['Message'] . "<td>" . $row['Date'];
					echo "</td></tr>";
				}
				echo "</table>";
				break;
			case "mailing":
				echo "<h2>Mailing Lists</h2>";
				echo "<table border='1'>";
				$result = mysql_query("SELECT * FROM mailingLists");
				while($row = mysql_fetch_array($result)){
					echo "<tr><td>";
					echo $row['Name'];
					echo "</td></tr>";
				}
				echo "</table>";
				break;
			case "events":
				//display events
				echo "<h2>Events</h2>";
				echo "<table border='1'>";
				$result = mysql_query("SELECT * FROM events");
				while($row = mysql_fetch_array($result)){
					echo "<tr><td>";
					echo $row['KeyWord'] . "<td>" . $row['Date'] . "<td>" . $row['Comments'];
					echo "</td></tr>";
				}
				echo "</table>";
				break;
			case "members":
				//display members
				echo "<h2>Members</h2>";
				echo "<table border='1'>";
				$result = mysql_query("SELECT * FROM members");
				while($row = mysql_fetch_array($result)){
					echo "<tr><td>";
					echo $row['Name'] . "<td>" . $row['Phone'] . "<td>" . $row['Email']. "<td>" . $row['Date'];
					echo "</td></tr>";
				}
				echo "</table>";
				break;
		}
	}
	//members at a meeting or mailing list
	if ($_POST["members_meetings"]!=""){
		echo "<h2>Members at a Meeting/Mailing List ". $_POST["members_meetings"] ."</h2>";
		echo "<table border='1'>";
		$query = "	SELECT DISTINCT M.Name, M.Email 
					FROM members M, emails E
					WHERE 	M.Phone = E.Phone
							AND TRIM(E.Message) LIKE \"".$_POST["members_meetings"]."%\"";
		//echo $query;
		$result = mysql_query($query);
		while($row = mysql_fetch_array($result)){
			echo "<tr><td>";
			echo $row['Name'] . "<td>"  . $row['Email'];
			echo "</td></tr>";
		}
		echo "</table>";
	}
	//individual person
	if ($_POST["person"]!=""){
		echo "<h2>Individual Person's Events: ". $_POST["person"] ."</h2>";
		echo "<table border='1'>";
		$query = 	"SELECT members.Name, members.Email, events.KeyWord, events.Date
					FROM events, emails, members
					WHERE 	members.Name LIKE \"%".$_POST["person"]."%\" 
							AND members.Phone = emails.Phone
							AND events.KeyWord = emails.Message"; 
		//echo $query;
		$result = mysql_query($query);
		while($row = mysql_fetch_array($result)){
			echo "<tr><td>";
			echo $row['Name'] . "<td>" . $row['Email'] . "<td>" . $row['KeyWord'];
			echo "</td></tr>";
		}
		echo "</table>";
	}
	
	mysql_close();
	?>
	
	
	<h5><br>&copy; 2012 Brian Goodacre </h5>
    </body>
</html>