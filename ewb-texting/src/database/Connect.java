package database;

import java.sql.*;

public class Connect
{
	public static Connection getConnection(){
		DB db = new DB();
        Connection conn=db.dbConnect("jdbc:mysql://88.198.43.24/ewbtexts", "ewbrutgers", "11ruEWB12");
        return conn;
	}
	public static void closeConnection(Connection conn) throws SQLException{
		conn.close();
	}

}

class DB
{
        public DB() {}

        public Connection dbConnect(String db_connect_string,
          String db_userid, String db_password)
        {
                try
                {
                        Class.forName("com.mysql.jdbc.Driver").newInstance();
                        Connection conn = DriverManager.getConnection(
                          db_connect_string, db_userid, db_password);
      
                        System.out.println("connected");
                        return conn;
                        
                }
                catch (Exception e)
                {
                        e.printStackTrace();
                        return null;
                }
        }
};