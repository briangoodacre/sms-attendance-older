DROP TABLE emails;
DROP TABLE members;
DROP TABLE events;
DROP TABLE mailingLists;

CREATE TABLE members(
Phone VARCHAR(20),
Date DATE,
Name VARCHAR(50),
Email VARCHAR(50),
PhoneEmail VARCHAR(100),
PRIMARY KEY(Phone)
);

CREATE TABLE emails(
id INTEGER AUTO_INCREMENT,
Date DATE,
EmailAddress VARCHAR(100),
Phone VARCHAR(20),
Subject VARCHAR(50),
Message VARCHAR(1000),
PRIMARY KEY (id)
);

CREATE TABLE events(
KeyWord VARCHAR(25),
Date DATE,
Comments VARCHAR(50),
PRIMARY KEY(KeyWord)
);

CREATE TABLE mailingLists(
Name VARCHAR(25),
PRIMARY KEY(Name)
);


