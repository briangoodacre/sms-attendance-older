package database;

import java.sql.*;
import java.util.*;
import java.util.Date;

import javax.mail.Address;

public class Inserting {
	
	public static void updateEmail(Date timestamp, Address from, String subject, String message) throws SQLException{
		//timestamp -> Date
		//parse with tokenizer to convert to sql requirements
		StringTokenizer st1 = new StringTokenizer(timestamp.toString()," ");
		int i=1;
		String year_=""; String date_="";
		while(st1.hasMoreTokens()){
			  String s=st1.nextToken();
			  //System.out.println(s);
			  if(i==3)
				  date_ = new String(s);
			  if(i==6)
				  year_ = new String(s);
			  i++;
			  }
		String date = year_+"-"+(timestamp.getMonth()+1)+"-"+date_;
		//System.out.println("Date: "+date);
		
		//from -> Phone and EmailAddress
		String from1 = from.toString();
		StringTokenizer st = new StringTokenizer(from1,"\"<>");
		String Phone="", EmailAddress="";
		int count=0;
		//System.out.println("Address:"+from);
		while(st.hasMoreTokens()){//tokenize it
			  String s=st.nextToken();
			  //System.out.println("s:" + s);
			  if (count==0)
				  Phone=new String(s);
			  if (count==2)
				  EmailAddress=new String(s);
			  count++;
		}
		//subject -> Subject (nothing to do)
		//message -> Message convert to all lowercase
		message = message.toLowerCase();
		
		
		//Inserting
		Connection conn = Connect.getConnection();
		Statement statement = conn.createStatement();
		String query = "" +
				"INSERT INTO emails " +
				"(Date, EmailAddress, Phone, Subject, Message) " +
				"VALUES ('"+date+"', '"+EmailAddress+"', '"+Phone+"', '"+subject+"', '"+message+"')";
		statement.executeUpdate(query);
		Connect.closeConnection(conn);
		
		//see if anything extra needs to happen for the message
		readMessage(date,Phone,message,EmailAddress);
	}

	//uses the message and does actions based off of it
	public static void readMessage(String date, String Phone, String message, String phoneEmail) throws SQLException{
		//database information
		Connection conn = Connect.getConnection();
		Statement statement = conn.createStatement();
		String query = "";
		
		//tokenize the input
		ArrayList<String> tokens = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(message, " ");
		while(st.hasMoreTokens()){//tokenize it and add to ArrayList
			  String s=st.nextToken();
			  s.toLowerCase();
			  tokens.add(new String(s)); //copy and add it
		}
		
		//do actions based on keywords
		//admin
		if(tokens.get(0).equals("admin")){
			//TODO- error checks for size of the input 
			if(tokens.get(1).equals("event") && tokens.size()>=3){
				//create new event
				query = "" +
						"INSERT INTO events " +
						"(KeyWord,Date,Comments) " +
						"VALUES ('"+tokens.get(2)+"', '"+date+"', '"+tokens.get(3)+"')";
				try{
					statement.executeUpdate(query);
				}
				catch (Exception e){
					System.out.println("Already inserted");
				}
			}
			if(tokens.get(1).equals("mail")  && tokens.size()>=3){
				//create new mailing list
				query = "" +
						"INSERT INTO mailingLists " +
						"(Name) " +
						"VALUES ('"+tokens.get(2)+"')";
				try{
					statement.executeUpdate(query);
				}
				catch (Exception e){
					System.out.println("Already inserted");
				}
			}
			else{
				//error
			}
		}
		//register
		else if(tokens.get(0).equals("register") || tokens.get(0).equals("Register")){
			try{
			query = "" +
					"INSERT INTO members " +
					"(Phone,Date,Name,Email,PhoneEmail) " +
					"VALUES ('"+Phone+"', '"+date+"', '"+tokens.get(1)+" "+tokens.get(2)+"', '"+tokens.get(3)+"', '"+phoneEmail+"')";
			statement.executeUpdate(query);
			}
			catch (Exception e){
				System.out.println("Already inserted this member");
			}
		}
		else{
			//Creating extra tables for event memebers and mailing list members is redundant because this information can be obtained from joins with the members table
			//data is already present in the emails table
			/*
			String temp;
			boolean keepGoing=true; //for the while loop
			query = "SELECT KeyWord FROM events";
			statement.executeQuery(query);
			ResultSet rs = statement.getResultSet();
			while(rs.next()&&keepGoing){//
				temp = rs.getString("KeyWord");
				if (tokens.get(0)==temp){
					query = "" +
							"INSERT INTO eventMembers " +
							"(KeyWord,Phone,Date) " +
							"VALUES ('"+temp+"', '"+Phone+"', '"+date+"')";
					keepGoing=false;
				}
			}
			if(keepGoing){//mailing list check
				query = "SELECT Name FROM mailingLists";
				statement.executeQuery(query);
				ResultSet rs = statement.getResultSet();
				while(rs.next()&&keepGoing){
					temp = rs.getString("Name");
					if (tokens.get(0)==temp){
						query = "" +
								"INSERT INTO eventMembers " +
								"(KeyWord,Phone,Date) " +
								"VALUES ('"+temp+"', '"+Phone+"', '"+date+"')";
						keepGoing=false;
					}
				}
			}
			*/
			//check if for mailing list
			//check if the event
			//else return error
		}
		Connect.closeConnection(conn);
	}
}
